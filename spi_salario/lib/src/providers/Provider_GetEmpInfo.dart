import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:spi_salario/src/models/EmpleadoModel.dart';
import 'package:spi_salario/src/models/TokenModel.dart';

class ProviderGetEmpInfo {
  //API EMPLOYEE ZOHO API
  String _url =
      "http://gro.apisipsalario.co/EmployeePayrolls/getEmployeeInfoBy.json";

  Future<List<Empleado>> getEmployeeInfo(Token token, String rfc) async {
    List<Empleado> empleados = new List();
    //Parametros body,Auth Bearer parse, auth
    Map<String, dynamic> body = {"RFC": "GOVY710610N14"};
    String tokenizer = "Bearer " + token.token;
    Map<String, String> auth = {"Authorization": tokenizer};

    //Petición
    var peticion = await http.post(_url, body: body, headers: auth);

    if (peticion.statusCode != 200) return null;

    var decode = jsonDecode(peticion.body);

    List<dynamic> result = decode["payload"];

    result.forEach((e) {
      final empleado = Empleado.fromJson(e);
      empleados.add(empleado);
    });

    return empleados;
    //Decode
    // Map<String, dynamic> decodeData = jsonDecode(peticion.body);
    // print(decodeData);
  }
}
