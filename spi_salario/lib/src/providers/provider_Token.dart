import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:spi_salario/src/models/TokenModel.dart';

class ProviderToken {
  //ZOHO API
  String _url = "http://gro.apisipsalario.co/Users/token.json"; //API USER AUTH

  Future<Token> getToken(String password, String email) async {
    //Parametros
    Map<String, dynamic> authData = {
      "username": "efren.casillas",
      "password": "lkpark27",
      "vendor": "1"
    };

    //Petición
    var peticion = await http.post(_url, body: authData);

    //Estatus ok sino error
    if (peticion.statusCode != 200) return Token.error(true);

    Map<String, dynamic> decodeData = jsonDecode(peticion.body);
    //Conversion de Map a objeto Token
    Token token = Token.fromJson(decodeData["data"]);

    return token;
  }
}
