/*
Rutas hacia las vistas(pages) para el MaterialApp del main.dart
Para evitar aglomerar el main con todas las rutas.
 */
import 'package:flutter/material.dart';
import 'package:spi_salario/src/pages/solucitudesPages/solicitud_page.dart';
import 'package:spi_salario/src/pages/SP_mensajesPages/mensajes_page.dart';
import 'package:spi_salario/src/cone_mysql/conexion.dart';
import 'package:spi_salario/src/pages/consulta_capacidadPages/capacidad_page.dart';
import 'package:spi_salario/src/pages/consulta_capacidadPages/detalleOtorgamiento_page.dart';
import 'package:spi_salario/src/pages/consulta_capacidadPages/detallePrereserva_Page.dart';
import 'package:spi_salario/src/pages/consulta_capacidadPages/detalleReserva_page.dart';
import 'package:spi_salario/src/pages/carga_page.dart';
import 'package:spi_salario/src/pages/detalles1_page.dart';
import 'package:spi_salario/src/pages/detalles2_page.dart';
import 'package:spi_salario/src/pages/home_page.dart';
import 'package:spi_salario/src/pages/login_page.dart';
import 'package:spi_salario/src/pages/logout_page.dart';
import 'package:spi_salario/src/pages/registro_page.dart';
import 'package:spi_salario/src/pages/prestamosPages/consultarPrestamos_page.dart';
import 'package:spi_salario/src/pages/prestamosPages/detallesPrestamo_page.dart';
import 'package:spi_salario/src/pages/restaurar_page.dart';

Map<String, WidgetBuilder> rutas() {
  return {
    'login': (BuildContext context) => LoginPage(),
    'home': (BuildContext context) => HomePage(),
    'registro': (BuildContext context) => RegistroPage(),
    'detalles1': (BuildContext context) => Detalles1Page(),
    'detalles2': (BuildContext context) => Detalles2Page(),
    'restaurar': (BuildContext context) => ReataurarPage(),
    'carga': (BuildContext context) => CargaPage(),
    'logout': (BuildContext context) => LogoutPage(),
    'consultarPrestamos': (BuildContext context) => ConsultarPrestamosPage(),
    'detallesPrestamo': (BuildContext context) => DetallesPrestamoPage(),
    //RUTAS DE CONSULTA CAPACIDAD//
    'consultaCapacidad': (BuildContext context) => CapacidadPage(),
    'dReserva': (BuildContext context) => DReservaPage(),
    'dPreReserva': (BuildContext context) => DPreReservaPage(),
    'dOtorgamiento': (BuildContext context) => DotorgamientoPage(),
    'solicitudes': (BuildContext context) => SolicitudPage(),
    'mensaje': (BuildContext context) => MensajePage(),
    //API mySQL
    'getdata': (BuildContext context) => Getdata(),
  };
}
