class Token {
  Token({this.token, this.err});

  String token;
  bool err = false;

  factory Token.fromJson(Map<String, dynamic> json) => Token(
        token: json["token"],
      );

  factory Token.error(bool err) => Token(token: "error", err: true);

  Map<String, dynamic> toJson() => {
        "token": token,
      };
}
