import 'dart:convert';

// Empleado empleadoFromJson(String str) => Empleado.fromJson(json.decode(str));

// String empleadoToJson(Empleado data) => json.encode(data.toJson());

class Empleado {
  Empleado({
    this.curpNom,
    this.rfcNom,
    this.claveEmpleado,
    this.nombreCompleto,
    this.capacidad,
    this.banco,
    this.cuenta,
    this.plaza,
    this.noDeCheque,
    this.sobreEndeudamiento,
    this.tipoDeNomina1,
    this.claveInterbancaria,
    this.negativoPositivo,
    this.statusEmpleado,
    this.aODeIngreso,
    this.quincenaDeIngreso,
    this.capacidadDisponible,
  });

  String curpNom;
  String rfcNom;
  String claveEmpleado;
  String nombreCompleto;
  String capacidad;
  String banco;
  String cuenta;
  String plaza;
  String noDeCheque;
  String sobreEndeudamiento;
  String tipoDeNomina1;
  String claveInterbancaria;
  String negativoPositivo;
  String statusEmpleado;
  String aODeIngreso;
  String quincenaDeIngreso;
  String capacidadDisponible;

  factory Empleado.fromJson(Map<String, dynamic> json) => Empleado(
        curpNom: json["CURPNom"],
        rfcNom: json["RFCNom"],
        claveEmpleado: json["Clave_Empleado"],
        nombreCompleto: json["Nombre_Completo"],
        capacidad: json["Capacidad"],
        banco: json["Banco"],
        cuenta: json["Cuenta"],
        plaza: json["Plaza"],
        noDeCheque: json["No_de_cheque"],
        sobreEndeudamiento: json["Sobre_Endeudamiento"],
        tipoDeNomina1: json["Tipo_de_Nomina1"],
        claveInterbancaria: json["Clave_Interbancaria"],
        negativoPositivo: json["Negativo_Positivo"],
        statusEmpleado: json["Status_Empleado"],
        aODeIngreso: json["A_o_de_Ingreso"],
        quincenaDeIngreso: json["Quincena_de_Ingreso"],
        capacidadDisponible: json["Capacidad_Disponible"],
      );

  Map<String, dynamic> toJson() => {
        "CURPNom": curpNom,
        "RFCNom": rfcNom,
        "Clave_Empleado": claveEmpleado,
        "Nombre_Completo": nombreCompleto,
        "Capacidad": capacidad,
        "Banco": banco,
        "Cuenta": cuenta,
        "Plaza": plaza,
        "No_de_cheque": noDeCheque,
        "Sobre_Endeudamiento": sobreEndeudamiento,
        "Tipo_de_Nomina1": tipoDeNomina1,
        "Clave_Interbancaria": claveInterbancaria,
        "Negativo_Positivo": negativoPositivo,
        "Status_Empleado": statusEmpleado,
        "A_o_de_Ingreso": aODeIngreso,
        "Quincena_de_Ingreso": quincenaDeIngreso,
        "Capacidad_Disponible": capacidadDisponible,
      };
}
