import 'package:flutter/material.dart';
import 'package:spi_salario/src/bloc/provider.dart';
import 'package:spi_salario/src/preferencias_usuarios/preferencias_usuario.dart';
import 'package:spi_salario/src/utils/rutas.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final prefs = new PreferenciasUsuario();
  await prefs.initPrefs();

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final prefs = new PreferenciasUsuario();
    print(prefs.token); //imprime el token
    print(prefs.email); //imprime el email
    return Provider(
        child: MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'SPI-SALARIO',
      initialRoute: 'login',
      routes: rutas(),
      theme: ThemeData(primaryColor: Colors.blue[800]),
    ));
  }
}
